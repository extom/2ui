# 2ui

A 2D graphical user interface meant to overlay 3D displays. Most 1st-person games display a health bar, menus, alerts and other text on top of the game itself. Intended to work in browsers and Electron with three.js for a variety of outputs and menus.

### Inputs

* Keybindings, for dependent hotkeys
* Plain CSS, for editing settings
* Cursor, set and forget 2D selections

### Eventual Outputs

* Vector arrow for wayfinding
* Select locations
* Event-finder
* Marketplace
* ...just about everything that's not easily 3D

It's not meant as a replacement for dat.gui, or as a WoW hotkey mess. To avoid clutter, there should only be a few active at a time. VR view will need a set distance to emulate a hud. Mobile view will be touch-friendly.